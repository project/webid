<?php

/**
 * Implements hook_default_rules_configuration().
 */
function webidauth_default_rules_configuration() {
  $configs['webidauth_webid_guest'] = rules_import('{ "webidauth_webid_guest" : {
    "LABEL" : "Grant WebID guest role to users logged in with WebID",
    "PLUGIN" : "reaction rule",
    "REQUIRES" : [ "rules", "webidauth" ],
    "ON" : [ "user_login_webidauth" ],
    "DO" : [
      { "drupal_message" : { "message" : "You have been successfully authenticated with your WebID." } },
      { "user_add_role_webidauth_machine_name" : { "account" : [ "account" ], "role" : "webid guest" } }
    ]
  }
}');

  return $configs;
}
