<?php

/**
 * Implementation of hook_rules_event_info().
 */
function webidauth_rules_event_info() {
  return array(
    'user_login_webidauth' => array(
      'label' => t('User has logged in with WebID'),
      'group' => t('User'),
      'variables' => array(
        'account' => array('type' => 'user', 'label' => t('logged in user with WebID')),
      ),
      'access callback' => 'rules_user_integration_access',
    ),
  );
}

/**
 * Implements hook_rules_action_info() on behalf of the user module.
 */
function webidauth_rules_action_info() {
  $defaults = array(
   'parameter' => array(
      'account' => array(
        'type' => 'user',
        'label' => t('User'),
        'description' => t('The user whose role should be changed.'),
        'save' => TRUE,
      ),
      'role' => array(
        'type' => 'text',
        'label' => t('Role'),
        'sanitize' => TRUE,
        'translatable' => TRUE,
      ),
    ),
    'group' => t('User'),
    'access callback' => 'rules_user_role_change_access',
  );
  $items['user_add_role_webidauth_machine_name'] = $defaults + array(
    'label' => t('Add user role (machine name)'),
    'base' => 'webidauth_action_user_add_role_machine_name',
  );
  return $items;
}

/**
 * Action: Adds roles to a particular user (machine name).
 */
function webidauth_action_user_add_role_machine_name($account, $role) {
  if ($account->uid || !empty($account->is_new)) {
    $webid_guest_role = user_role_load_by_name($role);
    // Create role if it doesn't exist yet.
    if (!$webid_guest_role) {
      $webid_guest_role = new stdClass();
      $webid_guest_role->name = $role;
      $webid_guest_role->weight = 3;
      user_role_save($webid_guest_role);
    }

    // Set role to account.
    $account->roles[$webid_guest_role->rid] = $role;

    if (!empty($account->is_new) && $account->uid) {
      // user_save() inserts roles after invoking hook_user_insert() anyway, so
      // we skip saving to avoid errors due saving them twice.
      return FALSE;
    }
  }
  else {
    return FALSE;
  }
}
